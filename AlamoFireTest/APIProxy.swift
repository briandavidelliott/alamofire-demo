//
//  APIProxy.swift
//  AlamoFireTest
//
//  Created by Brian on 4/7/17.
//  Copyright © 2017 Unplugged Systems, LLC. All rights reserved.
//

import Foundation

protocol APIProxy {
    /**
     A method which will make an API call
     
     -parameter router: The APIRouter object containing the details of the call to be made
     - parameter completion: The closure to be called when the API call completes. This will be executed asynchronously.
     */
   // func makeNetworkCall(router: APIRouter, completion: (response: APIResponse) -> ()) -> APIRequest
}
